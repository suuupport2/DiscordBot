FROM python:3.11-alpine
LABEL authors="n0fail"

RUN apk update && \
    apk add --no-cache ffmpeg opus opus-dev bash

ENV LIBOPUS_PATH="/usr/lib/libopus.so"

COPY requirements.txt /opt
RUN pip install -r /opt/requirements.txt

COPY src/ /opt/src
WORKDIR /opt/src

ARG DISCORD_TOKEN
ENV DISCORD_TOKEN=${DISCORD_TOKEN}

CMD python bot.py